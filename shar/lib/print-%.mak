# -------------------------------------------------------------------
# Synopsis: debugging target for GNU make from book: "GNU Make
# Unleashed" by John Graham-Cumming; "Makefile Debugging" chapter.
#
#   This "make include file" allows you to for example, dump the
#   value of $(HOME), by passing make the target:
#
#     print-HOME
#
# --------------------------------------------------------------------
print-%:
	@echo $* = "$($*)"
	@echo $*\'s origin is $(origin $*)

__print-%:
	@echo $*:"$($*)"
  # terse output

_print-%:
	@echo $*: \[$($*)]
  # terse output
