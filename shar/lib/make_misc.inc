define bltok =
@echo
@echo PWD: $$PWD;ls -logh $@
@echo @@built ok: $@
@echo
endef

define bltok_phony =
@echo
@echo PWD: $$PWD
@echo @@built ok: "PHONY [$@]"
@echo
endef

define blding =
@echo -e "\n@@[$@] build $$(date)"
@if [[ -e $@ ]]; then  set -x;: Target [$@] b4 bld:;ls -logt $@;fi
@if [[ -e $@ ]]; then  echo;fi
@set -- $^; if [[ $$# -gt 0 ]] ;then set -x;: [$@] Dependencies:;ls -logrt $^ || true;fi
@echo @@"[$@]" build started:
@echo
endef

define blding_phony =
@echo -e "\n@@Starting PHONY [$@] build $$(date)"
@echo
endef

2_hours_ago_atmost.ts : 2_hours_ago

2_hours_ago::
	@touch --date "2 hours ago" $@
	if ! (find 2_hours_ago_atmost.ts -newer $@ |egrep -q 2_hours_ago_atmost.ts) ;then \
        (\
            set -x;\
            : 2_hours_ago_atmost.ts is too old or missing.;\
            touch 2_hours_ago_atmost.ts;\
        )\
    else\
        echo 2_hours_ago_atmost.ts is up to date, ie less than 2 hours old.;\
    fi

# 'newer' is deprecated, since "::" works.
.PHONY: newer
newer: ;

_debug_info = $(shell  \
		{ \
			echo "_debug_info:	MAKE: $(MAKE)";\
			echo "make started: $$(date)";\
			echo "CURDIR: $(CURDIR)";\
			echo "MAKEFILE_LIST: $(MAKEFILE_LIST)";\
			echo "MAKELEVEL: $(MAKELEVEL)";\
			echo "MAKEFLAGS: $(MAKEFLAGS)";\
			echo "MAKECMDGOALS: $(MAKECMDGOALS)";\
			echo ;\
		} >&2 \
	)

# Ex:
# $(if true, $(_debug_info))

