#!/bin/false -meant-to-be-included-in-makefile

# Synopsis: make recipes for a type of patching
#   , saved in macros created by make defines

# Description: now used to apply patches to my work and home journals.
#   These text files keep growing in size, so it makes sense to only 
#   diffs (patches) periodically.

# make recipes

# ====================================================================
# Copyright (c) 2011 Tom Rodman <Rodman.T.S@gmail.com>
# --------------------------------------------------------------------

# -- Software License --
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ---

# --------------------------------------------------------------------
# $Source: /usr/local/7Rq/package/cur/make-2010.10.02/shar/lib/RCS/patch_long-static_ref-file.mak._m4,v $
# $Date: 2012/01/21 19:25:50 $ GMT    $Revision: 1.4 $
# --------------------------------------------------------------------

define mk_patch_4_log-static_ref_file
	ref_file=$< ;\
	cksum=$$(md5sum < $$ref_file );\
  updated_file=$$(set $^; echo $$2);\
  ( echo -e $$ref_file md5sum: $$cksum \\n;\
	  set -x;command diff -Naur $$ref_file $$updated_file \
  ) >$@ || :
endef

define patch_long-static_ref-file
	patch=$<;\
	ref_pn=$$(set -- $^; echo $$2 );\
	ref_bn=$${ref_pn#*/};\
  expected_cksum=$$( awk /$$ref_bn' md5sum:/ {print $$3}' $$patch );\
 	actual_cksum=$$( set -- $$(md5sum < $$ref_pn ); echo $$1 );\
  [[ $$expected_cksum = $$actual_cksum ]] || \
  { echo expected: $$expected_cksum actual: $$actual_cksum; exit 1;} ;\
 	cp -f $$ref_pn $@;\
 	patch < $$patch
endef

